<!DOCTYPE html>
<html>
<head>
	<title>PHP-3</title>
</head>
<body>

<?php
echo "<h3> Soal 1</h3>";
function tentukan_nilai($number)
{
	if($number<=100 && $number>=85){
		return "Sangat Baik"."<br>";
	}else if($number<85 && $number>=70){
		return "Baik"."<br>";  
	}else if($number<70 && $number>=60){
		return "Cukup"."<br>";
	}else if($number<60 && $number>=0){
		return "Kurang"."<br>";
	}else{
		return "Inputan Salah!!"."<br>";
	}
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


echo "<br>";
echo "<h3> Soal 2</h3>";
function ubah_huruf($string){
	$j = strlen($string);
	for($i=0; $i<$j; $i++){
		$n_string[$i] = chr(ord($string[$i])+1);
	}
	return implode($n_string)."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu


echo "<br>";
echo "<h3> Soal 3</h3>";

	function tukar_besar_kecil($string){
	$output="";
	$abjad="abcdefghijklmnopqrstuvwxyz";
	$j = strlen($string);
	for($i=0; $i<$j; $i++){

	if(ctype_upper($string[$i])){

		$output.= strtolower($string[$i]);

	}else {

		$output.= strtoupper($string[$i]);
	}
	}
	return $output."<br>";

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"


?>

</body>
</html>